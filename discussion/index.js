// Creating a simple server in Express.js

const express = require('express');
const app = express();

const port = 4000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// mock database

let users = [
	{
		email: "attackTitan@mail.com",
		username: "thisIsEren",
		password: "notABird",
		isAdmin: false
	},
	{
		email: "mrClean@mail.com",
		username: "AckermanLevi",
		password: "stillAlive",
		isAdmin: true
	},
	{
		email: "redScarf@mail.com",
		username: "Mikasa12",
		password: "whereIsEren",
		isAdmin: false
	}
];

let loggedUser;

app.get('/', (req, res) => {

	res.send('Hello World')

});

/*
	Mini-Activity
	1. Make a route /hello as an endpoint
	2. Send a message saying hello from batch 145
*/

app.get('/hello', (req, res) => {
	res.send('Hello from Batch 145')
});

app.post('/', (req, res) => {
	
	console.log(req.body)
	res.send(`Hello! I am ${req.body.name}, my age is ${req.body.age}. I could be described as ${req.body.description} `)

});

/*
	Mini-Activity:
	1. Change the message from the POST method
	2. res.send = Hello, I am <nameFromRequest>, my age is <ageFromRequest>. I could be described as <descriptionFromRequest>
	Send your POSTMAN output in Hangouts
*/


// register route
app.post('/users1', (req, res) => {
	
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered`)

});

// log in route

app.post('/users1/login', (req, res) => {

	console.log(req.body);

	let foundUser = users.find((user) => {
		return user.username === req.body.username && user.password === req.body.password
	})

	if(foundUser !== undefined) {
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});
			
			foundUser.index = foundUserIndex

			loggedUser = foundUser

			res.send('Thank you for logging in')
		
	} else {
			loggedUser = foundUser
			res.send('Sorry, wrong credentials')
	}

});


// Change password
app.put('/users1/change-password', (req, res) => {
	
	let message;
	
	for(let i = 0; i < users.length; i++){
		
		if(req.body.username == users[i].username) {
			users[i].password = req.body.password
			
			message = `User ${req.body.username}'s password has been changed`

			break;

		} else {

			message = `User does not exist`

		}
	}

	res.send(message)

})



// START OF S31 ACTIVITY


// GET route to access /home route
app.get('/home', (req, res) => {

	res.send('Welcome to the home page')

});



// GET all users in database
app.get('/users', (req, res) => {

	res.send(users);

});



// DELETE user from database
app.delete('/delete-user', (req, res) => {

		let deleteUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
		};
    
    users.splice(1, 1);
		console.log(users);

		res.send(`User ${req.body.username} has been deleted`)

});

// END OF ACTIVITY



app.listen(port, () => console.log(`Server running at port ${port}`));